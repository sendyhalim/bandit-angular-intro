(function() {
	var todosFixture = [{
		id: 1,
		title: 'First todo',
		completed: false,
		user: {
			firstName: 'Sendy',
			lastName: 'halim',
			pet: {
				firstName: 'someone',
				lastName: 'something'
			}
		}
	}, {
		id: 2,
		title: 'Second todo',
		completed: true,
		user: {
			firstName: 'John',
			lastName: 'Doe',
			pet: {
				firstName: 'Bam',
				lastName: 'Bang'
			}
		}
	}];

	var todoApiFactory = function($q, TodoTransformer) {
		var todoApi = {};

		todoApi.getTodos = function() {
			var deferred = $q.defer();

			todosFixture = TodoTransformer.transformArray(todosFixture);

			deferred.resolve(todosFixture);
			return deferred.promise;
		};

		todoApi.postTodo = function(newTodo) {
			var deferred = $q.defer();

			todosFixture.push(newTodo);

			deferred.resolve({success: true});

			return deferred.promise;
		};

		todoApi.deleteTodo = function(id) {
			var deferred = $q.defer();

			var deletedTodo;

			todosFixture.forEach(function(todo, i) {
				if (todo.id == id) {
					todosFixture.splice(i, 1);
					deleteTodo = todo;
				}
			});

			return deferred.promise.then(function() {
				return {success: true, deletedTodo: deletedTodo};
			});
		};

		todoApi.putTodo = function(updatedTodo) {
			var deferred = $q.defer();

			todosFixture.forEach(function(todo, i) {
				if (todo.id === updatedTodo.id) {
					todosFixture[i].title = updatedTodo.title;
				}
			});

			deferred.resolve(updatedTodo);

			return deferred.promise;
		};

		return todoApi;
	};

	todoApiFactory.$inject = ['$q', 'TodoTransformer'];

	angular.module('TodoApp')
		.factory('todoApi', todoApiFactory);
})();
