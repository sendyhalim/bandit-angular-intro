angular.module('TodoApp')
.config([
	'$stateProvider',
	'$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		$stateProvider.state('home', {
			url: '/',
			controller: 'TodoCtrl',
			templateUrl: 'templates/todo/index.tpl.html'
		});

		$urlRouterProvider.otherwise('/');
	}
]);
