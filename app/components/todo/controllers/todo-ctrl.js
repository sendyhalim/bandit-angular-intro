(function() {
	var TodoCtrl = function($scope, todoApi) {
		var currentId = 0;
		var assignTodos = function() {
			todoApi.getTodos().then(function(todos) {
				$scope.todos = todos;
				currentId = todos.length;
			});
		};

		$scope.editMode = false;

		$scope.setToEditMode = function(toBeUpdatedTodo) {
			$scope.currentTodo = toBeUpdatedTodo;
			$scope.editMode = true;
		};

		$scope.currentTodo = {};


		$scope.updateTodo = function() {
			todoApi.putTodo($scope.currentTodo).then(function(res) {
				$scope.currentTodo = {};
				$scope.editMode = false;
			});
		};

		$scope.createTodo = function() {
			$scope.currentTodo.completed = false;
			$scope.currentTodo.id = ++currentId;

			todoApi.postTodo($scope.currentTodo).then(function(res) {
				assignTodos();
			});

			$scope.currentTodo = {};
		};

		$scope.deleteTodo = function(id) {
			todoApi.deleteTodo(id).then(function(res) {
				assignTodos();
			});
		};

		assignTodos();
	};


	TodoCtrl.$inject = ['$scope', 'todoApi'];

	angular.module('TodoApp')
		.controller('TodoCtrl', TodoCtrl);
})();
