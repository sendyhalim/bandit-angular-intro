(function() {
	var todoTransformerFactory = function(BaseTransformer, UserTransformer) {
		var todoTransformer = BaseTransformer.create();
		var userTransformer = Object.create(UserTransformer);

		userTransformer.propertyKey = 'user';

		todoTransformer.transformers.push(userTransformer);

		todoTransformer.methods = {
			getFirstWord: function() {
				return this.title.split(' ')[0];
			}
		};

		return todoTransformer;
	};

	todoTransformerFactory.$inject = ['BaseTransformer', 'UserTransformer'];

	angular.module('TodoApp')
		.factory('TodoTransformer', todoTransformerFactory);
})();
