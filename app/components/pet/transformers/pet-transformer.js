(function() {
	var petTransformerFactory = function(BaseTransformer) {
		var petTransformer = BaseTransformer.create();

		petTransformer.methods = {
			getFullName: function() {
				return this.firstName + '  is my pet!';
			}
		};

		return petTransformer;
	};

	angular.module('TodoApp')
		.factory('PetTransformer', petTransformerFactory);
})();
