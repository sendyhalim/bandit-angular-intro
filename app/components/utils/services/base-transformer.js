angular.module('TodoApp')
.factory('BaseTransformer', [
		function () {
			var BaseTransformer = function () {
				this.transformers = [];
				this.methods = {};
			};

			// run each transformer to transform model
			BaseTransformer.prototype.transform = function (model) {
				_.forEach(this.transformers, function (transformer) {
					var key = transformer.propertyKey;
					// if transformer is set to transform the model's property..
					if (key) {
						// check if the property is an object (make sure it is an object first then we can use 'in' operator)
						// if it is not an object and not exist in the model then just return
						if (!(model[key] === Object(model[key]) && (key in model))) {
							return;
						}
						// if model's property is an Array, then we will iterate each of them and transform it
						if (model[key] instanceof Array) {
							_.forEach(model[key], function (prop) {
								prop = transformer.transform(prop);
							});// end _.forEach()
						} else {
							model[key] = transformer.transform(model[key]);
						}
					}
					// if no property key was set, then just transform the model itself
					else {
						model = transformer.transform(model);
					}
				});

				// reserve own methods (higher priority)
				model = angular.extend(model, this.methods);

				return model;
			};

			BaseTransformer.prototype.transformArray = function(arr) {
				var self = this;
				var transformed = [];

				//convert to array first
				_.forEach(arr, function(a) {
					transformed.push(self.transform(a));
				});

				return transformed;
			};

			return {
				create: function () {
					return new BaseTransformer();
				}
			};
		}
]);
