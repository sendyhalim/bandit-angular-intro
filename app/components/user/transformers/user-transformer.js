(function() {
	var userTransformerFactory = function(BaseTransformer, PetTransformer) {
		var userTransformer = BaseTransformer.create();
		var petTransformer = Object.create(PetTransformer);

		petTransformer.propertyKey = 'pet';

		userTransformer.transformers.push(petTransformer);

		userTransformer.methods = {
			getFullName: function() {
				return this.firstName + '  ' + this.lastName;
			}
		};

		return userTransformer;
	};

	userTransformerFactory.$inject = ['BaseTransformer', 'PetTransformer'];

	angular.module('TodoApp')
		.factory('UserTransformer', userTransformerFactory);
})();
