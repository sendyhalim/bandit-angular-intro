#Preparing for project

```
//Install Node.js, add node js to your environment variables

//Install grunt
sudo npm install -g grunt

//Install grunt-cli
sudo npm install -g grunt-cli

//Install bower
sudo npm install -g bower

// clone the project repo
git clone https://bitbucket.org/sendyhalim/bandit-angular-intro.git

// change directory to project repo
cd bandit-angular-intro

// install npm dependencies
sudo npm install

// install bower dependencies
bower install

// then there should be node_modules and bower_components directories in your project repo
// open public/index.html with browser to see project
// app/bootstrap is the bootstrap of the project
// app/components is js app components
// app/assets is LESS assets
// to build run grunt-build
// to watch directories run grunt
```
