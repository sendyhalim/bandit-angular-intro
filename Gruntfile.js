module.exports = function(grunt) {
  var bowerDir = 'bower_components';
  var nodeModuleDir = 'node_modules';

  var libraries = [
    'jquery/dist/jquery.min.js',
    'bootstrap/dist/js/bootstrap.min.js',
    'angular/angular.min.js',
    'angular-ui-router/release/angular-ui-router.min.js',
    'lodash/lodash.min.js'
  ];

  var appFiles = [
    'app/bootstrap/**/*.js',
    'app/components/**/*.js'
  ];

  //----- APPENDING libraries with bowerDir
  for (var i = 0, totalLib = libraries.length; i < totalLib; i++) {
    libraries[i] = bowerDir + '/' + libraries[i];
  }

  // combining libraries(first) then appFiles
  var filesToConcat = libraries.concat(appFiles);

  var styles = [
		'bower_components/bootstrap/dist/css/bootstrap.min.css',
    '<%= mainCss %>'
  ];

  //Initializing the configuration object
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    mainCss: 'public/dist/app.css',
    concat: {
      options: {
          separator: "\n"
      },
      js: {
        src: filesToConcat,
        dest: 'public/dist/app.js'
      },
      styles: {
        src: styles,
        dest: 'public/dist/style.css'
      }
    },
    less: {
			development: {
				files: {
					"public/dist/app.css": "app/assets/init.less"
				}
			}
		},
    autoprefixer: {
      styles: {
        src: 'public/dist/app.css',
        dest: 'public/dist/app.css'
      },
    },
    watch: {
      app: {
        files: [
					'app/**/*.js',
					'app/assets/**/*.less',
					'public/templates/**/*.html',
				],
        tasks: [
        	'less:development',
					'autoprefixer:styles',
					'concat:styles',
					'concat:js',
        ],//tasks to run
      }
    },
  });


  // // Plugin loading
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-autoprefixer');

  // Task definition
  grunt.registerTask('default', ['watch:app']);

  grunt.registerTask('build', [
		'less:development',
		'autoprefixer:styles',
    'concat:styles',
    'concat:js',
  ]);
};
